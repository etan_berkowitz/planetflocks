// reducers take the state and the action (keep in mind reducer composition)
function orbit(state=[], action) {
  switch(action.type) {
    case 'UPDATE_CURRENT_ORBIT':
      return action.orbit
    default:
      return state;
  }
}

export default orbit;