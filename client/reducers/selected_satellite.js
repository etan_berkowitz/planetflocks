// reducers take the state and the action (keep in mind reducer composition)
function selected_satellite(state=[], action) {
  switch(action.type) {
    case 'UPDATE_SELECTED_SATELLITE':
      return {
        id: action.id,
        images_collected: action.images_collected,
        last_command: action.last_command,
        deorbit_dt: action.deorbit_dt
      }
    default:
      return state;
  }
}

export default selected_satellite;