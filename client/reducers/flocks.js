// reducers take the state and the action (keep in mind reducer composition)
function flocks(state=[], action) {
  switch(action.type) {
    case 'JSON_DATA_SUCCESS':
      console.log('reducer loading json data endpoint')
      return action.doves;
    case 'ADD_SATELLITE':
      console.log('reducer adding satellite');
      return [
      ...state, {
        id: action.id,
        active: action.active,
        color: action.color,
        images_collected: action.images_collected,
        last_command: action.last_command,
        deorbit_dt: action.deorbit_dt
      }]
    default:
      return state;
  }
}

export default flocks;