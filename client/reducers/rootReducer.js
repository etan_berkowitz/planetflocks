import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import flocks from './flocks';
import filter_term from './filter_term';
import selected_satellite from './selected_satellite';

const rootReducer = combineReducers({flocks, filter_term, selected_satellite, routing: routerReducer });

export default rootReducer;
