// reducers take the state and the action (keep in mind reducer composition)
function filter_term(state=[], action) {
  switch(action.type) {
    case 'FILTER_SATELLTIES':
      return action.filter_term
    default:
      return state;
  }
}

export default filter_term;