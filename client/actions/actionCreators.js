// does not hot reload
export function addSatellite(id, active, color, images_collected, last_command, deorbit_dt) {
  console.log("actionCreator dispatching ADD_SATELLITE")
  return {
    type: 'ADD_SATELLITE',
    id,
    active,
    color,
    images_collected,
    last_command,
    deorbit_dt
  }
}

export function filterSatellites(filter_term) {
    console.log("actionCreator dispatching FILTER_SATELLTIES")
    return {
        type: 'FILTER_SATELLTIES',
        filter_term
    }
}

export function itemDataSuccess(doves) {
    console.log("actionCreator dispatching JSON_DATA_SUCCESS")
    return {
        type: 'JSON_DATA_SUCCESS',
        doves
    };
}

export function updateSatelliteSelect(id, images_collected, last_command, deorbit_dt) {
    console.log("actionCreator dispatching UPDATE_SELECTED_SATELLITE")
    return {
        type: 'UPDATE_SELECTED_SATELLITE',
        id,
        images_collected,
        last_command,
        deorbit_dt
    }
}

export function getItemData(url) {
    return (dispatch) => {
        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                return response;
            })
            .then((response) => {
              return response.json();
            })
            .then((doves) => {
              dispatch(itemDataSuccess(doves));
            });
    };
}