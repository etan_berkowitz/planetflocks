import React from 'react';

// import react router deps
import { Router, Route, IndexRoute } from 'react-router';
// binds react and redux (which can be used with anything not just react)
import { Provider } from 'react-redux';
// curly bracket for named export instead of default export, exported redux history instead of browserHistory
import store, { history } from './store';

// Import Components
import ReduxConnection from './ReduxConnection';
import Home from './components/Home'
import Dove from './components/Dove'
import Search from './components/Search'

export default class Routes extends React.Component {
  constructor(props) {
    super(props);
  }

  render() { 
    return (
      <Provider store={store}>
        <Router history={history}>
          <Route path="/" component={ReduxConnection}>
            <IndexRoute path="/home" component={Home}></IndexRoute>
            <Route path="/search" component={Search}></Route>
            <Route path="/dove" component={Dove}></Route>
          </Route>
        </Router>
      </Provider>
    )
  }
}
