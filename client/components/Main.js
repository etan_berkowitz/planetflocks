import React from 'react';
import { Link } from 'react-router';
import { getItemData } from '../actions/actionCreators';
import FlockRow from './FlockRow';
import FlockItem from './FlockItem';

export default class Main extends React.Component {
  componentDidMount() {
    this.props.getItemData('http://localhost:3000/doves');
  }

  render() {
    // Before a user clicks a satellite for information, default text is set. Once a satellite is clicked, default text is never shown again.
    return (
      <div>
        <h1 className="nav-contents-center">
          <Link to="/">Birds of a Feather</Link>
        </h1>

        <div className="nav-contents-center nav-bar">
          <Link className="nav-item" to="/dove" activeClassName="active">\Add A Satellite/</Link>
          <Link className="nav-item" to="/search" activeClassName="active">\Satellite Search/</Link>
        </div>
        <div className="monitor-container">
          <div className="monitor-contents">
            {this.props.selected_satellite.id ? (
              <p>{`${this.props.selected_satellite.id} has collected ${this.props.selected_satellite.images_collected} images.`}
              <br />{`Most Recent Command: ${this.props.selected_satellite.last_command}`}
              <br />{`Deorbit Date: ${this.props.selected_satellite.deorbit_dt}`}</p>
            ) : (
              <p className="default-text">Click a satellite</p>
            )}
          </div>
        </div>
        {/* because just this.props.childen doesn't get you access to the children's props  */}
        { React.cloneElement(this.props.children, this.props) }        
      </div>
    );
  }
}
