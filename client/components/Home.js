import React from 'react';
import { Link } from 'react-router';
import FlockRow from './FlockRow';
import FlockItem from './FlockItem';

export default class Home extends React.Component {
  constructor(props) {
      super(props);
    }
  
  filterSatellites(flockData) {
    // We're searching for any satellite's properties that contains any part of the text a user inputs
    const filterTerm = this.props.filter_term.toLowerCase(),
          filteredFlock = flockData.filter((satellite) => {
            for (let key in satellite) {
              if (satellite[key].toString().toLowerCase().includes(filterTerm)) {
                return true;
              }
            }
            return false;
          });

    return filteredFlock;
  }

  renderSatellite(flockData) {
    let currentRow = [],
        flockRows = [],
        filteredFlock = this.props.filter_term.length ? this.filterSatellites(flockData) : flockData;

    for (let i = 0; i < filteredFlock.length; i++) {
        // Assign a number 1-5, in order, to each flock item to give them the proper CSS class
        filteredFlock[i].active ? filteredFlock[i]["orbitClass"] = `satellite-move-${(i % 5) + 1}` : filteredFlock[i]["orbitClass"] = "deactivate-satellite-color";

      if (currentRow.length < 4) {
        currentRow.push(filteredFlock[i])
      } else if (currentRow.length === 4) {
        flockRows.push(<FlockRow {...this.props} flockRowData={currentRow} key={i}/>)
        currentRow = [filteredFlock[i]];
      }
    }

    // Putting that last row in there
    if (currentRow.length) {
      flockRows.push(<FlockRow {...this.props} flockRowData={currentRow} key={filteredFlock.length}/>)
    }

    return (
      <div className="container">
        {flockRows}
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.renderSatellite(this.props.flocks)}
      </div>
    );
  }
}
