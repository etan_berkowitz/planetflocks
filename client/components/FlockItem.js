import React from 'react';
import { Link } from 'react-router';
import { updateSatelliteSelect } from '../actions/actionCreators';

export default class FlockItem extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSatelliteClick (data) {
    this.props.updateSatelliteSelect(data.id, data.images_collected, data.last_command, data.deorbit_dt);
  }

  render() {
    return ( 
      <div key={this.props.index} onClick={() => this.handleSatelliteClick(this.props.data)} className="satellite col-md-3">
        <div className={`satellite-img ${this.props.data.orbitClass}`}>
          <div className="satellite-color" style={{backgroundColor: `${this.props.data.active ? this.props.data.color : ""}`}}>
          </div>
        </div>
      </div>
    );
  }
}
