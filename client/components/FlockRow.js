import React from 'react';
import { Link } from 'react-router';
import FlockItem from './FlockItem';

export default class FlockRow extends React.Component {
  constructor(props) {
    super(props);
  }

  renderOrbit (flockItemData) {
    // Will render the movement CSS if the satellite ACTIVE field is true, otherwise, spit out a deactivated satellite CSS
    if (flockItemData.active) {
      return `satellite-move-${this.props.orbit}`
    } else {
      return "deactivate-satellite-color"
    }
  }

  render() {
    return (
      <section className="row" key={this.props.index}>
        {this.props.flockRowData.map((flockItemData, index) => <FlockItem {...this.props} data={flockItemData} key={index}/>)}
      </section>
    );
  }
}