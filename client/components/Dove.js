import React from 'react';
import { Link } from 'react-router';
import Home from './Home';

export default class Dove extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSubmit(e) {
    e.preventDefault();

    const id = this.refs.id.value,
          active = JSON.parse(this.refs.active.value),
          color = this.refs.color.value,
          images_collected = this.refs.images.value,
          last_command = this.refs.command.value,
          deorbit_dt = this.refs.deorbit.value;

    this.props.addSatellite(id, active, color, images_collected, last_command, deorbit_dt);
    
    // Clear input fields
    this.refs.form.reset()
  }

  render() {
    return (
      <div>
        <form ref="form" className="form nav-contents-center" onSubmit={this.handleSubmit.bind(this)}>
          <input type="text" ref="id" placeholder="id" />
          <select ref="active">
            <option value="false">Satellite Inactive</option>
            <option value="true">Satellite Active</option>
          </select>
          <input type="color" ref="color" placeholder="color" />
          <input type="number" ref="images" placeholder="images" min="0"/>
          <input type="text" ref="command" placeholder="command" />
          <input type="date" ref="deorbit" placeholder="deorbit date"/>
          <input type="submit" /> {/* submit input needed for enter key to work */}
        </form>

        <Home {...this.props} />
      </div>
    );
  }
}
