import React from 'react';
import { Link } from 'react-router';
import Home from './Home';

export default class Search extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSubmit(e) {
    // User can filter satellites by any property they might have. Check db.json for examples of key/values.
    e.preventDefault();

    const filter_term = this.refs.filter.value;

    this.props.filterSatellites(filter_term);
  }

  render() {
    return (
      <div>
        <form className="form nav-contents-center" >
          <input type="text" onChange={this.handleSubmit.bind(this)} ref="filter" placeholder="Enter Search Term" />
          <input type="submit" hidden /> {/* submit input needed for enter key to work */}
        </form>
        <Home {...this.props} />
      </div>
    );
  }
}
